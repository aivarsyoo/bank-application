from celery import shared_task
import requests
from .models import Account


@shared_task
def recurring_payment(data):
    if Account.has_sufficient_balance(data['debit_account'], data['amount']):
        api_url = f"{data['debit_bank_link']}bank_api/before_transfer/"
        response = requests.post(api_url, data=data)
        if response.status_code == 200:
            print("Transaction success")
            return "Transaction success"
        else:
            print("Transaction failed")
            return "Transaction failed"
    else:
        print("Not enough money")
        return "Not enough money"
