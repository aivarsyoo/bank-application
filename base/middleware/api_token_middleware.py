from django.middleware.common import CommonMiddleware
from django.shortcuts import get_object_or_404
import sys
sys.path.append('../')
from base.models import Bank

class APITokenMiddleware(CommonMiddleware):
    def process_view(self, request, view_func, view_args, view_kwargs):
        if view_func.__name__ in ['do_transfer', 'check_account_exists']:
            api_token = request.headers.get('API-Token')
            if not api_token:
                raise Exception("API token is required")
            bank = get_object_or_404(Bank, api_token=api_token)
            view_kwargs['bank'] = bank
