from django import forms
from . models import Customer, Account, Bank
from django.contrib.auth.models import User
from django.core.validators import MinLengthValidator
from django.core.exceptions import ObjectDoesNotExist


class UserForm(forms.ModelForm):
    username = forms.CharField(help_text='')
    email = forms.EmailField(max_length=50)
    first_name = forms.CharField(required=True, max_length=20, validators=[
                                 MinLengthValidator(2, 'First name must contain at least 2 characters')])
    last_name = forms.CharField(required=True, max_length=20, validators=[
                                MinLengthValidator(2, 'Last name must contain at least 2 characters')])

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email')

    def clean(self):
            cleaned_data = super().clean()
            email = cleaned_data.get('email')
            username = cleaned_data.get('username')

            if User.objects.filter(email=email).exclude(username=username).exists():
                msg = 'A user with that email already exists.'
                self.add_error('email', msg)           
        
            return self.cleaned_data

class CustomerForm(forms.ModelForm):
    phone_number = forms.CharField(widget=forms.TextInput(attrs={'autocomplete': 'off','pattern':'[0-9]+', 'title':'Enter numbers Only '}))
    class Meta:
        model = Customer
        fields=['rank','phone_number']
        widgets = {
        'rank': forms.Select(attrs={'class': 'form-control'}),
    }

class AccountForm(forms.ModelForm):
    class Meta:
        model = Account
        fields=['account_type']
        widgets = {
        'account_type': forms.Select(attrs={'class': 'form-control'})
        }

class TransferForm(forms.Form):
    amount  = forms.DecimalField(label='Amount', max_digits=10)
    debit_account = forms.ModelChoiceField(label='Debit Account', queryset=Customer.objects.none())
    debit_text = forms.CharField(label='Debit Account Text', max_length=25)
    bank = forms.ModelChoiceField(label='Credit Bank', queryset=Bank.objects)
    credit_account = forms.IntegerField(label='Credit Account Number')
    credit_text = forms.CharField(label='Credit Account Text', max_length=25)

    def clean(self):
        super().clean()

        # Ensure credit account exist
        """ credit_account = self.cleaned_data.get('credit_account')
        try:
            Account.objects.get(pk=credit_account)
        except ObjectDoesNotExist:
            self._errors['credit_account'] = self.error_class(['Credit account does not exist.']) """

        # Ensure positive amount
        """ if self.cleaned_data.get('amount') < 0:
            self._errors['amount'] = self.error_class(['Amount must be positive.']) """

        return self.cleaned_data

class TickerForm(forms.Form):
    ticker = forms.CharField(label='Enter stock symbol', max_length=5)

class MoneyRequestForm(forms.Form):
    amount  = forms.DecimalField(label='Amount', max_digits=10)
    debit_account = forms.ModelChoiceField(label='Your Account', queryset=Customer.objects.none())
    credit_account = forms.IntegerField(label='Request Account')
    text = forms.CharField(label='Reason', max_length=25)

    def clean(self):
        super().clean()

        # Ensure credit account exist
        credit_account = self.cleaned_data.get('credit_account')
        try:
            Account.objects.get(pk=credit_account)
        except ObjectDoesNotExist:
            self._errors['credit_account'] = self.error_class(['Credit account does not exist.'])

        # Ensure positive amount
        if self.cleaned_data.get('amount') < 0:
            self._errors['amount'] = self.error_class(['Amount must be positive.'])

        return self.cleaned_data

class RecurrentPaymentForm(forms.Form):
    name = forms.CharField(label='Name for your recurrent payment', max_length=25)
    amount  = forms.DecimalField(label='Amount', max_digits=10)
    debit_account = forms.ModelChoiceField(label='Debit Account', queryset=Customer.objects.none())
    debit_text = forms.CharField(label='Debit Account Text', max_length=25)
    bank = forms.ModelChoiceField(label='Credit Bank', queryset=Bank.objects)
    credit_account = forms.IntegerField(label='Credit Account Number')
    credit_text = forms.CharField(label='Credit Account Text', max_length=25)
    time_value = forms.IntegerField(label='Every', widget=forms.TextInput(attrs={'class': 'time-value'}))
    time_freq = forms.ChoiceField(
        choices=[
            ('seconds', 'Seconds'),
            ('minutes', 'Minutes'),
            ('hours', 'Hours'),
            ('days', 'Days'),
            ('months', 'Months'),
        ],
        label='',
    )

    def clean(self):
        super().clean()
        return self.cleaned_data