import requests

headers = {
    'Content-Type': 'application/json',
    'Authorization': 'Token 2139b747ecda138c589200b77cfe02e25fe83851'
}

def get_meta_data(ticker):
    url = 'https://api.tiingo.com/tiingo/daily/{}'.format(ticker)
    response = requests.get(url, headers=headers)
    return response.json()

def get_price_data(ticker):
    url = 'https://api.tiingo.com/tiingo/daily/{}/prices'.format(ticker)
    response = requests.get(url, headers=headers)
    return response.json()[0]
