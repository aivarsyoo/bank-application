# Generated by Django 4.0.2 on 2023-01-09 06:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0015_alter_bank_port'),
    ]

    operations = [
        migrations.AddField(
            model_name='bank',
            name='api_token',
            field=models.CharField(default=1, max_length=50),
            preserve_default=False,
        ),
    ]
