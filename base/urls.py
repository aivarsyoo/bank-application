from django.urls import path
from . import views
from django.contrib.auth.views import LogoutView

urlpatterns = [
    path('register_otp/', views.register_two_factor, name='register_otp'),
    path('', views.login_user, name='login'),
    path("logout/", LogoutView.as_view(next_page="login"), name="logout"),

    path("users/", views.CustomersList.as_view(), name="users"),
    path('create_customer/', views.create_customer, name='create_customer'),
    path('customer_details/<int:pk>/', views.customer_details, name='customer_details'),
    path('create_account/<int:user>/<int:customer>/', views.create_account, name='create_account'),
    path('customer_accounts_list/<int:pk>/', views.list_accounts, name='list_accounts'),
    path('transaction_details/<int:transaction>/', views.transaction_details, name='transaction_details'),

    path('account_details/<int:pk>/<int:user>/', views.account_details, name='account_details'),
    path('make_transfer/', views.prepare_transfer, name='make_transfer'),
    path('prepare_transfer/', views.prepare_transfer, name='prepare_transfer'),
    path('do_transfer/', views.do_transfer, name='do_transfer'),
    path('check_account_exists/', views.check_account_exists, name='check_account_exists'),
    path('make_loan/', views.make_loan, name='make_loan'),
    path('dashboard/', views.dashboard, name='dashboard'),

    path('search_stocks/', views.search_stocks, name='search_stocks'),
    path('search_stocks/<str:tid>', views.stock_result, name='stock_result'),

    path('money_request/', views.make_money_request, name='money_request'),
    path('delete_money_request/<int:id>', views.delete_request, name='delete_money_request'),

    path('make_recurrent_payment/', views.prepare_recurrent_payment, name='make_recurrent_payment'),
    path('customer_recurrent_payments/', views.customer_recurrent_payments, name='customer_recurrent_payments'),
    path('list_recurrent_payments/<int:customer_pk>/', views.list_recurrent_payments, name='list_recurrent_payments'),
    path('delete_recurrent_payment/<int:task_pk>/', views.delete_recurrent_payment, name='delete_recurrent_payment'),
]