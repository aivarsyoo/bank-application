from django.shortcuts import render, redirect, get_object_or_404, reverse
from django.views.generic.list import ListView
from secrets import token_urlsafe
from django.db import IntegrityError
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseBadRequest
from django.core.exceptions import PermissionDenied
from django.contrib.auth.views import LoginView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import authenticate, login as auth_login
from decimal import Decimal
from django.contrib.auth.models import User
import qrcode
import qrcode.image.svg
from io import BytesIO
from django_otp import devices_for_user
from django_otp.plugins.otp_totp.models import TOTPDevice
from django_otp import match_token
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from .forms import UserForm, CustomerForm, AccountForm, TransferForm, TickerForm, MoneyRequestForm, RecurrentPaymentForm
from django.contrib.auth.decorators import login_required
from . models import Customer, Account, Transaction, MoneyRequest, Bank
from .tiingo import get_meta_data, get_price_data
import requests
from django.views.decorators.csrf import csrf_exempt
from django_celery_beat.models import PeriodicTask, IntervalSchedule
from django.utils import timezone
import json



def login_user(request):
    if request.user.is_authenticated:
        if request.user.is_staff:
            return redirect('users')
        else:
            return redirect('dashboard')

    if request.method == 'GET':
        return render(request, 'base/login.html')

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        otp_token = request.POST['token']
        user = authenticate(
            request=request, username=username, password=password)

        if user is not None:
            device_match = match_token(user=user, token=otp_token)
            print("DEVICE MATCH :::: ", device_match)

            if device_match is None:
                return render(request, 'base/login.html', {'error': 'Error - authentication token was wrong, or you have not registered two factor auth'})
            else:
                auth_login(request, user)
                if request.user.is_staff:
                    return HttpResponseRedirect(reverse('users'))
                else:
                    return HttpResponseRedirect(reverse('dashboard'))

        else:
            return render(request, 'base/login.html', {'error': 'Error - User not found'})

@login_required
def dashboard(request):
    assert not request.user.is_staff, 'Staff user routing customer view.'

    accounts = request.user.customer.accounts
    context = {
        'accounts': accounts,
    }
    return render(request, 'base/dashboard.html', context)


class CustomersList(LoginRequiredMixin, ListView):
    model = Customer
    context_object_name = "users"
    template_name = "base/customers_list.html"


def getCurrentBankObject(request):
    port = request.get_port()
    bank = Bank.objects.get(port=port)
    return bank
    

@login_required
def create_customer(request): # because we need to insert data in 2 tables with the same form, need to create objects for that to define what goes where
    assert request.user.is_staff, 'Customer user routing staff view.'

    if request.method == 'POST': # if we click submit for the form, this happens
        user_form = UserForm(request.POST)
        customer_form = CustomerForm(request.POST)
        if user_form.is_valid() and customer_form.is_valid(): # validation from forms.py and models.py
            username = user_form.cleaned_data['username']
            first_name = user_form.cleaned_data['first_name']
            last_name = user_form.cleaned_data['last_name']
            email = user_form.cleaned_data['email']
            password = token_urlsafe(16)
            rank = customer_form.cleaned_data['rank']
            phone_number = customer_form.cleaned_data['phone_number']
            bank = getCurrentBankObject(request)
            try:
                with transaction.atomic():
                    user = User.objects.create_user( # we use Model.objects.create for only inserting the data, not updating
                        username=username,           # populate user table
                        password=password,
                        email=email,
                        first_name=first_name,
                        last_name=last_name
                    )
                    Customer.objects.create( # populate customer table
                        user=user,
                        rank=rank,
                        phone_number=phone_number,
                        bank=bank
                    )
                    # return staff_customer_details(request, user.pk)
                    return redirect("users")
            except IntegrityError:
                context = {
                    'title': 'Database Error',
                    'error': 'User could not be created.'
                }
                return render(request, 'base/error.html', context)
    else: # we don't click anything yet, so just show me the forms
        user_form = UserForm()
        customer_form = CustomerForm()
    context = {
        'user_form': user_form,
        'customer_form': customer_form,
    }
    return render(request, 'base/create_customer.html', context)

@login_required
def customer_details(request, pk):
    assert request.user.is_staff, 'Customer user routing staff view.'

    customer = get_object_or_404(Customer, pk=pk) # calls the given model and get object from that, pk tells which customer to use
    if request.method == 'GET': # thats for displaying info in the fields
        user_form = UserForm(instance=customer.user) # customer.user tells which user to access
        customer_form = CustomerForm(instance=customer) # in customer variable above we defined already which customer to access
    elif request.method == 'POST': # pass the fields to update customer info
        user_form = UserForm(request.POST, instance=customer.user) # here we need the instance to let know which user to update
        customer_form = CustomerForm(request.POST, instance=customer)
        if user_form.is_valid() and customer_form.is_valid():
            user_form.save()
            customer_form.save()
    account_form = AccountForm() # just show me empty form for creating a new account
    context = {
        'customer': customer,
        'user_form': user_form,
        'customer_form': customer_form,
        'account_form': account_form,
    }
    return render(request, 'base/customer_details.html', context)

@login_required
def create_account(request, user, customer):
    assert request.user.is_staff, 'Customer user routing staff view.'

    if request.method == 'POST':
        account_form = AccountForm(request.POST)
        if account_form.is_valid():
            Account.objects.create(
                user=User.objects.get(pk=user), 
                account_type=account_form.cleaned_data['account_type']
                )
    return HttpResponseRedirect(reverse('customer_details', args=(customer,))) # args in the url
    # trailing comma is required for single-item tuples to disambiguate defining a tuple from an expression surrounded by parentheses
    # HttpResponseRedirect prevents data from being posted twice if a user hits the Back button

@login_required
def list_accounts(request, pk):
    assert request.user.is_staff, 'Customer user routing staff view.'

    customer = get_object_or_404(Customer, pk=pk)
    accounts = customer.accounts
    context = {
        'accounts': accounts,
    }
    return render(request, 'base/customer_accounts_list.html', context)

@login_required
def account_details(request, pk, user):
    account = get_object_or_404(Account, user=user, pk=pk)
    context = {
        'account': account
    }
    return render(request, 'base/account_details.html', context)

@login_required
def transaction_details(request, transaction):
    movements = Transaction.objects.filter(transaction=transaction)
    if not request.user.is_staff:
        if not movements.filter(account__in=request.user.customer.accounts):
            raise PermissionDenied('Customer is not part of the transaction.')
    context = {
        'movements': movements,
    }
    return render(request, 'base/transaction_details.html', context)

@login_required
def prepare_transfer(request):
    if request.method == 'POST':
        form = TransferForm(request.POST)
        form.fields['debit_account'].queryset = request.user.customer.accounts # a collection of data from a database. A QuerySet is built up as a list of objects.
        if form.is_valid():                                                    # queryset allows to filter and order the data.
            amount = form.cleaned_data['amount']
            debit_account = Account.objects.get(pk=form.cleaned_data['debit_account'].pk)
            debit_text = form.cleaned_data['debit_text']
            credit_account = form.cleaned_data['credit_account']
            credit_text = form.cleaned_data['credit_text']

            debit_bank = getCurrentBankObject(request)
            debit_bank_link = f"{debit_bank.link}:{debit_bank.port}/"
            credit_bank = Bank.objects.get(pk=form.cleaned_data['bank'].pk)
            credit_bank_link = f"{credit_bank.link}:{credit_bank.port}/"

            # Define tokens for cookies
            debit_bank_token = debit_bank.api_token
            credit_bank_token = credit_bank.api_token

            debit_account_counterparty = request.user.customer.full_name()

            if amount <= 0:
                context = {
                    'title': 'Error',
                    'error': 'Amount should be a positive number'
                }
                return render(request, 'base/error.html', context)

            if Account.has_sufficient_balance(debit_account.pk, amount):
                response = api_before_transfer(request, amount, debit_account.pk, debit_text, credit_account, credit_text, debit_bank_link, credit_bank_link, debit_bank_token, credit_bank_token, debit_account_counterparty)
                response.set_cookie('debit_bank_token', debit_bank_token)
                response.set_cookie('credit_bank_token', credit_bank_token)
                return response
            else:
                context = {
                    'title': 'Error',
                    'error': 'Not enough money'
                }
                return render(request, 'base/error.html', context)
    else:
        form = TransferForm()
        form.fields['debit_account'].queryset = request.user.customer.accounts # all customer accounts choice
        context = {
        'form': form,
        }
    return render(request, 'base/make_transfer.html', context)


@login_required
def api_before_transfer(request, amount, debit_account, debit_text, credit_account, credit_text, debit_bank_link, credit_bank_link, debit_bank_token, credit_bank_token, debit_account_counterparty):
    data = {
        'amount': amount,
        'debit_account': debit_account,
        'debit_text': debit_text,
        'credit_account': credit_account,
        'credit_text': credit_text,
        'debit_bank_link': debit_bank_link,
        'credit_bank_link': credit_bank_link,
        'debit_bank_token': debit_bank_token,
        'credit_bank_token': credit_bank_token,
        'debit_account_counterparty': debit_account_counterparty
    }
    api_url = f"{debit_bank_link}bank_api/before_transfer/"
    response = requests.post(api_url, data=data)
    if response.status_code == 200:
        return HttpResponseRedirect(reverse('account_details', args=(debit_account, request.user.pk)))
    else:
        context = {
                    'title': 'Error',
                    'error': response.content
                }
        return render(request, 'base/error.html', context)

@csrf_exempt
def do_transfer(request, **kwargs):
    if request.method == 'POST':
        amount = request.POST.get('amount')
        debit_account = request.POST.get('debit_account')
        debit_text = request.POST.get('debit_text')
        credit_account = request.POST.get('credit_account')
        credit_text = request.POST.get('credit_text')
        debit_bank_link =  request.POST.get('debit_bank_link')
        credit_bank_link = request.POST.get('credit_bank_link')
        debit_account_counterparty = request.POST.get('debit_account_counterparty')
        credit_account_counterparty = request.POST.get('credit_account_counterparty')

        current_bank = getCurrentBankObject(request)
        current_bank_link = f"{current_bank.link}:{current_bank.port}/"
        
        try:
            cls = Transaction.transfer_2(amount, debit_account, debit_text, credit_account, credit_text, debit_bank_link, credit_bank_link, current_bank_link, debit_account_counterparty, credit_account_counterparty)
            return HttpResponse(cls)
        except Exception as e:
            return HttpResponseBadRequest(e)
    else:
        return HttpResponse("Wrong request method")

def check_account_exists(request, **kwargs):
    if request.method == 'GET':
        account_id = request.GET.get('account_id')
        account = Account.objects.get(pk=account_id)
        counterparty = account.user.customer.full_name()
        if not Account.check_account_exists(account_id):
            return HttpResponseBadRequest("Account doesn't exist")
        else:
            return HttpResponse(counterparty)
    else:
        return HttpResponseBadRequest("Wrong request method")


@login_required
def make_loan(request):
    assert not request.user.is_staff, 'Staff user routing customer view.'

    if not request.user.customer.can_make_loan:
        context = {
            'title': 'Create Loan Error',
            'error': 'Loan could not be completed.'
        }
        return render(request, 'base/error.html', context)
    if request.method == 'POST':
        request.user.customer.make_loan(Decimal(request.POST['amount']))
        return HttpResponseRedirect(reverse('dashboard'))
    return render(request, 'base/make_loan.html', {})

def register_two_factor(request):
    if request.method == 'GET':
        return render(request, 'base/totpauth.html')

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(
            request=request, username=username, password=password)

        if user:
            try:
                device = TOTPDevice.objects.get(
                    user=user, name=username) or None
                with transaction.atomic():
                    device.delete()
                    new_device = TOTPDevice.objects.create(
                        user=user, name=username)
                    new_device.save()
            except ObjectDoesNotExist:
                new_device = TOTPDevice.objects.create(
                    user=user, name=username)
                new_device.save()

            devices = devices_for_user(user)
            for device in devices:
                if isinstance(device, TOTPDevice):
                    print(device)
                    print(device.config_url)
                    context = {}
                    factory = qrcode.image.svg.SvgImage
                    img = qrcode.make(device.config_url,
                                      image_factory=factory, box_size=20)
                    stream = BytesIO()
                    img.save(stream)
                    context["svg"] = stream.getvalue().decode()
                    return render(request, 'base/login.html', {'device': device, 'context': context})
        else:
            return render(request, 'base/totpauth.html', {'error': 'wrong credentials'})

@login_required
def search_stocks(request):
    assert not request.user.is_staff, 'Staff user routing customer view.'
    if request.method == "POST":
        form = TickerForm(request.POST)
        if form.is_valid():
            ticker = request.POST["ticker"]
            return HttpResponseRedirect(ticker)
    else:
        form = TickerForm()
    return render(request, "base/search_stocks.html", {"form":form})

@login_required
def stock_result(request, tid):
    assert not request.user.is_staff, 'Staff user routing customer view.'
    context = {}
    context["ticker"] = tid
    context["meta"] = get_meta_data(tid)
    context["price"] = get_price_data(tid)
    return render(request, "base/stock_result.html", context)

@login_required
def make_money_request(request):
    assert not request.user.is_staff, 'Staff user routing customer view.'

    if request.method == 'POST':
        form = MoneyRequestForm(request.POST)
        form.fields['debit_account'].queryset = request.user.customer.accounts 
        if form.is_valid():                                                  
            amount = form.cleaned_data['amount']
            debit_account = Account.objects.get(pk=form.cleaned_data['debit_account'].pk)
            credit_account = Account.objects.get(pk=form.cleaned_data['credit_account'])
            text = form.cleaned_data['text']
            requester = request.user
            where_to_send = Account.objects.get(pk=form.cleaned_data['debit_account'].pk)
            MoneyRequest.transfer(amount, debit_account, credit_account, requester, text, where_to_send)
            return redirect('dashboard')
 
    else:
        form = MoneyRequestForm()
    form.fields['debit_account'].queryset = request.user.customer.accounts # all customer accounts choice
    context = {
        'form': form,
    }
    return render(request, 'base/money_request.html', context)

@login_required
def delete_request(request, id):
    assert not request.user.is_staff, 'Staff user routing customer view.'
    money_request = MoneyRequest.objects.get(id=id)
    money_request.delete()
    return HttpResponseRedirect(reverse('dashboard'))



@login_required
def prepare_recurrent_payment(request):
    if request.method == 'POST':
        form = RecurrentPaymentForm(request.POST)
        form.fields['debit_account'].queryset = request.user.customer.accounts
        if form.is_valid():
            name = form.cleaned_data['name']                                                   
            amount = form.cleaned_data['amount']
            debit_account = Account.objects.get(pk=form.cleaned_data['debit_account'].pk)
            debit_text = form.cleaned_data['debit_text']
            credit_account = form.cleaned_data['credit_account']
            credit_text = form.cleaned_data['credit_text']
            time_freq = form.cleaned_data['time_freq']
            time_value = form.cleaned_data['time_value']

            debit_bank = getCurrentBankObject(request)
            debit_bank_link = f"{debit_bank.link}:{debit_bank.port}/"
            credit_bank = Bank.objects.get(pk=form.cleaned_data['bank'].pk)
            credit_bank_link = f"{credit_bank.link}:{credit_bank.port}/"

            debit_bank_token = debit_bank.api_token
            credit_bank_token = credit_bank.api_token

            debit_account_counterparty = request.user.customer.full_name()

            if amount <= 0:
                context = {
                    'title': 'Error',
                    'error': 'Amount should be a positive number'
                }
                return render(request, 'base/error.html', context)

            data = {
                'amount': int(amount),
                'debit_account': debit_account.pk,
                'debit_text': debit_text,
                'credit_account': credit_account,
                'credit_text': credit_text,
                'debit_bank_link': debit_bank_link,
                'credit_bank_link': credit_bank_link,
                'debit_bank_token': debit_bank_token,
                'credit_bank_token': credit_bank_token,
                'debit_account_counterparty': debit_account_counterparty
            }

            interval_schedule, created = IntervalSchedule.objects.get_or_create(
            every=time_value, period=time_freq)

            task = PeriodicTask.objects.create(
                name=name,
                task='base.tasks.recurring_payment',
                interval=interval_schedule,
                start_time=timezone.now(),
                args=json.dumps([data])
            )
            task.save()
            return HttpResponseRedirect(reverse('customer_recurrent_payments'))
    else:
        form = RecurrentPaymentForm()
        form.fields['debit_account'].queryset = request.user.customer.accounts
        context = {
        'form': form,
        }
        return render(request, 'base/make_recurrent_payment.html', context)

@login_required
def customer_recurrent_payments(request):
    customer = request.user.customer
    context = {'customer': customer}
    print(customer.pk)
    return render(request, 'base/customer_recurrent_payments.html', context)

@login_required
def list_recurrent_payments(request, customer_pk):
    payments = PeriodicTask.objects.all()
    customer = Customer.objects.get(pk=customer_pk)
    user_payments = []

    for payment in payments:
        data = json.loads(payment.args)
        for item in data:
            bank = Bank.objects.get(api_token=item['credit_bank_token'])
            bank_name = bank.name
            account = Account.objects.get(pk=item['debit_account'])
            if account.user == customer.user:
                instance = {
                    "payment": payment,
                    "amount": item['amount'],
                    "debit_account": account,
                    'bank': bank_name,
                    "credit_account": item['credit_account']
                    }
                user_payments.append(instance)

    return render(request, 'base/list_recurrent_payments.html', {'payments': user_payments})

@login_required
def delete_recurrent_payment(request, task_pk):
    task = PeriodicTask.objects.get(pk=task_pk)
    task.delete()

    task_args = json.loads(task.args)
    account_id = task_args[0]['debit_account']
    account = Account.objects.get(pk=account_id)
    customer_pk = account.user.customer.pk

    if request.user.is_staff:
        return HttpResponseRedirect(reverse('customer_details', args=(customer_pk,)))
    else:
        return HttpResponseRedirect(reverse('customer_recurrent_payments'))