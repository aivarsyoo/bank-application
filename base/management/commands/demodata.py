from django.contrib.auth.models import User
from django.db.utils import IntegrityError
from base.models import Customer, Bank, Account, AccountTypes
from faker import Faker
from django.core.management.base import BaseCommand

fake = Faker()

def create_objects():
    # Create two banks
    Bank.objects.create(name="Bank 1", link="http://app", port="8000", api_token="TRVpCQvFQaXGctgxLYmIPJaUtMLfFG")
    Bank.objects.create(name="Bank 2", link="http://app2", port="8001", api_token="bGhrvtBvWYgdvKRixKsCrlfsmgRvNN")

    # Create a superuser
    try:
        User.objects.create_superuser(
            username="admin",
            password="password",
            email="admin@example.com",
            first_name=fake.first_name(),
            last_name=fake.last_name(),
        )
    except IntegrityError:
        pass

    # Create four customers
    for i in range(1, 5):
        fake_name = fake.first_name()
        user = User.objects.create_user(
            username=fake_name,
            first_name=fake_name,
            last_name=fake.last_name(),
            password="password",
            email=f"{fake_name}@example.com",
        )
        bank = Bank.objects.all().order_by("?").first()
        Customer.objects.create(
            user=user, phone_number=f"1234567{i}", bank=bank
        )

    # Create two current and two savings accounts for each customer
    for customer in Customer.objects.all():
        Account.objects.create(
            user=customer.user, account_type=AccountTypes.current_account
        )
        Account.objects.create(
            user=customer.user, account_type=AccountTypes.savings_account
        )

class Command(BaseCommand):
    def handle(self, *args, **options):
        create_objects()