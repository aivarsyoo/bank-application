from django.core.management.base import BaseCommand
from .demodata import create_objects

class Command(BaseCommand):
    def handle(self, *args, **options):
        create_objects()