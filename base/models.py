from django.db import models, transaction
from django.contrib.auth.models import User
from django.core.validators import MinLengthValidator
from django.db.models.query import QuerySet
from decimal import Decimal
from .errors import InsufficientFunds

class Bank(models.Model):
    name = models.CharField(max_length=255)
    link = models.CharField(max_length=255)
    port = models.CharField(max_length=4, unique=True)
    api_token = models.CharField(max_length=50)

    def __str__(self):
        return f'{self.name}'

class UID(models.Model):
    @classmethod
    @property
    def uid(cls):
        return cls.objects.create()

    def __str__(self):
        return f'{self.pk}'

class AccountTypes(models.TextChoices):
    current_account = "Current Account",
    savings_account = "Savings Account",
    loan_account = "Loan Account"

class Account(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    account_type = models.CharField(max_length=256, choices=AccountTypes.choices)

    @property
    def movements(self) -> QuerySet:
        return Transaction.objects.filter(account=self)

    @property
    def requests(self) -> QuerySet:
        return MoneyRequest.objects.filter(account=self).exclude(requester=self.user)

    @property
    def balance(self) -> Decimal:
        return self.movements.aggregate(models.Sum('amount'))['amount__sum'] or Decimal(0)

    @classmethod
    def has_sufficient_balance(cls, account_pk, amount):
        account = cls.objects.get(pk=account_pk)
        return account.balance >= amount

    @classmethod
    def check_account_exists(cls, account_id: int) -> bool:
        try:
            cls.objects.get(pk=account_id)
            return True
        except cls.DoesNotExist:
            return False

    def __str__(self):
        return f'{self.pk} :: {self.user} :: {self.account_type}'


class Rank(models.TextChoices):
    basic = "basic",
    silver = "silver",
    gold = "gold"

class Customer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone_number = models.CharField(max_length=8, unique=True, validators=[MinLengthValidator(8, 'Phone number must contain 8 characters')])
    rank = models.CharField(max_length=256, choices=Rank.choices, default=2)
    bank = models.ForeignKey(Bank, on_delete=models.CASCADE)

    def full_name(self) -> str:
        return f'{self.user.first_name} {self.user.last_name}'

    def username(self): return self.user.username

    def email(self): return self.user.email

    @property
    def accounts(self) -> QuerySet:
        return Account.objects.filter(user=self.user)

    @property
    def can_make_loan(self) -> bool:
        return self.rank != "basic" # need to repair this!!!!!!

    @property
    def default_account(self) -> Account:
        return Account.objects.filter(user=self.user).first()

    def make_loan(self, amount):
        assert self.can_make_loan, 'User rank does not allow for making loans.'
        assert amount >= 0, 'Negative amount not allowed for loan.'
        loan = Account.objects.create(user=self.user, account_type=AccountTypes.loan_account)
        Transaction.transfer(
            amount,
            loan,
            f'Loan paid out to account {self.default_account}',
            self.default_account,
            f'Credit from loan {loan.pk}: {loan.account_type}',
            is_loan=True
        )

    def __str__(self):
        return f'{self.user.pk} :: {self.user} :: {self.rank}'


class Transaction(models.Model):
    account     = models.ForeignKey(Account, on_delete=models.PROTECT)
    transaction = models.ForeignKey(UID, on_delete=models.PROTECT)
    amount      = models.DecimalField(max_digits=10, decimal_places=2)
    timestamp   = models.DateTimeField(auto_now_add=True, db_index=True)
    text        = models.TextField()
    counterparty = models.CharField(max_length=255)

    @classmethod
    def transfer(cls, amount, debit_account, debit_text, credit_account, credit_text, is_loan=False) -> int:
        assert amount >= 0, 'Negative amount not allowed for transfer.'
        with transaction.atomic():
            if debit_account.balance >= amount or is_loan:
                uid = UID.uid
                cls(amount=-amount, transaction=uid, account=debit_account, text=debit_text).save()
                cls(amount=amount, transaction=uid, account=credit_account, text=credit_text).save()
            else:
                raise InsufficientFunds
        return cls

    @classmethod
    def transfer_2(cls, amount, debit_account, debit_text, credit_account, credit_text, debit_bank_link, credit_bank_link, current_bank_link, debit_account_counterparty, credit_account_counterparty) -> int:
        amount = int(amount)
        assert amount >= 0, 'Negative amount not allowed for transfer.'
        if debit_bank_link == credit_bank_link:
            debit_account = Account.objects.get(pk=debit_account)
            credit_account = Account.objects.get(pk=credit_account)
            with transaction.atomic():
                uid = UID.uid
                cls(amount=-amount, transaction=uid, account=debit_account, text=debit_text, counterparty=credit_account_counterparty).save()
                cls(amount=amount, transaction=uid, account=credit_account, text=credit_text, counterparty=debit_account_counterparty).save()
            return cls
        elif debit_bank_link == current_bank_link:
            debit_account = Account.objects.get(pk=debit_account)
            with transaction.atomic():
                uid = UID.uid
                cls(amount=-amount, transaction=uid, account=debit_account, text=debit_text, counterparty=credit_account_counterparty).save()
            return cls
        else:
            credit_account = Account.objects.get(pk=credit_account)
            with transaction.atomic():
                uid = UID.uid
                cls(amount=amount, transaction=uid, account=credit_account, text=credit_text, counterparty=debit_account_counterparty).save()
            return cls


class MoneyRequest(models.Model):
    account     = models.ForeignKey(Account, on_delete=models.CASCADE)
    requester = models.ForeignKey(User, on_delete=models.CASCADE)
    transaction = models.ForeignKey(UID, on_delete=models.CASCADE)
    amount      = models.DecimalField(max_digits=10, decimal_places=2)
    timestamp   = models.DateTimeField(auto_now_add=True, db_index=True)
    text        = models.TextField()
    where_to_send = models.CharField(max_length=255)

    @classmethod
    def transfer(cls, amount, debit_account, credit_account, requester, text, where_to_send) -> int:
        assert amount >= 0, 'Negative amount not allowed for transfer.'
        with transaction.atomic():
                uid = UID.uid
                cls(amount=amount, transaction=uid, account=debit_account, text=text, requester=requester, where_to_send=where_to_send).save()
                cls(amount=amount, transaction=uid, account=credit_account, text=text, requester=requester, where_to_send=where_to_send).save()
        return uid

    def __str__(self):
        return f'{self.amount} :: {self.transaction} :: {self.timestamp} :: {self.account} :: {self.text}'



