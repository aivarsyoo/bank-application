Welcome to Bank App
===================

This application is a simple bank management system that allows users to create bank accounts, perform transactions, and manage the accounts. There are two instances of bank in this application, Bank 1 and Bank 2.

### Bank 1

The Bank 1 instance link is <http://172.105.86.7:8000/>
For the admin user of Bank 1, the credentials are as follows:

-   username: `admin`
-   password: `password`

For the normal users of Bank 1, the credentials are as follows:

-   username: `Maria`
-   password: `keabank123`

-   username: `Karen`
-   password: `keabank123`

### Bank 2

The Bank 2 instance link is http://172.105.86.7:8001/
For the admin user of Bank 2, the credentials are as follows:

-   username: `admin`
-   password: `password`

For the normal users of Bank 2, the credentials are as follows:

-   username: `Kirsten`
-   password: `keabank123`

-   username: `Samantha`
-   password: `keabank123`

### Note

Once logged in, you can perform various actions like creating accounts, deposit or withdraw money, transfer money to other accounts, view transaction history and more.
You can send money between banks, but can not request it from another instance.

Local Setup
============

1.  Build the Docker image: To build the Docker image for the Django app, you can run the command `make build` or `docker-compose build` in the root of your project directory. This command will create the necessary Docker image for your app.

2.  Run the services: To run the required services for your app, including Redis, Nginx, and both bank instances, you can run the command `make core-up` or `docker-compose up -d`. This command will start the containers for all the services.

3.  Entrypoint Script: The entrypoint script takes care of creating the databases if they don't exist, installing all the requirements, also running the servers for the app itself, and starting the Celery worker and Celery beat processes.

4.  Access the app in the browser: Once the services are running, you can access the app in the browser by navigating to `127.0.0.1:8000` for the first instance and `127.0.0.1:8001` for the second instance.

Please note that it is important to have a good understanding of how Docker and Docker Compose work before running these commands and make sure that the ports 8000, 8001 and 6379 are not in use by other services running on your machine