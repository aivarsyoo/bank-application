#!/bin/sh

DJANGO_SETTINGS_MODULE=bank_application.settings.bank2 python manage.py check
make bank2-makemigrations
make bank2-migrate
make bank2-demodata
DJANGO_SETTINGS_MODULE=bank_application.settings.bank2 python manage.py runserver 0.0.0.0:8001