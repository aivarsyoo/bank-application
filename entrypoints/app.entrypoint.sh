#!/bin/sh

DJANGO_SETTINGS_MODULE=bank_application.settings.bank1 python manage.py check
make bank1-makemigrations
make bank1-migrate
make bank1-demodata
DJANGO_SETTINGS_MODULE=bank_application.settings.bank1 python manage.py runserver 0.0.0.0:8000