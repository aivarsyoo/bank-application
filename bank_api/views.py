import requests
from django.http import HttpResponse, HttpResponseBadRequest
from django.db import transaction
from django.views.decorators.csrf import csrf_exempt

@csrf_exempt
def before_transfer(request):
    if request.method == 'POST':
        amount = request.POST.get('amount')
        debit_account = request.POST.get('debit_account')
        debit_text = request.POST.get('debit_text')
        credit_account = request.POST.get('credit_account')
        credit_text = request.POST.get('credit_text')
        debit_bank_link =  request.POST.get('debit_bank_link')
        credit_bank_link = request.POST.get('credit_bank_link')
        debit_bank_token = request.POST.get('debit_bank_token')
        credit_bank_token = request.POST.get('credit_bank_token')
        debit_account_counterparty = request.POST.get('debit_account_counterparty')
        response = prepare_transfer(request, amount, debit_account, debit_text, credit_account, credit_text, debit_bank_link, credit_bank_link, debit_bank_token, credit_bank_token, debit_account_counterparty)
        if response.status_code == 200:
            return HttpResponse(response)
        else:
            return HttpResponseBadRequest(response)
    else:
        return HttpResponseBadRequest(response)

def prepare_transfer(request, amount, debit_account, debit_text, credit_account, credit_text, debit_bank_link, credit_bank_link, debit_bank_token, credit_bank_token, debit_account_counterparty):
    bank_array = [{'link': debit_bank_link, 'token': debit_bank_token}, {'link': credit_bank_link, 'token': credit_bank_token}]
    # At first let's check that credit account exists
    api_url = f"{credit_bank_link}check_account_exists/"
    headers = {'API-Token': credit_bank_token}
    response = check_account(credit_account, api_url, headers)
    credit_account_counterparty = response.content.decode()
    data = {
        'amount': amount,
        'debit_account': debit_account,
        'debit_text': debit_text,
        'credit_account': credit_account,
        'credit_text': credit_text,
        'debit_bank_link': debit_bank_link,
        'credit_bank_link': credit_bank_link,
        'debit_account_counterparty': debit_account_counterparty,
        'credit_account_counterparty': credit_account_counterparty
    }
    # If exists, let's do the transfer
    if response.status_code == 200:
        responses = []
        if debit_bank_link == credit_bank_link:
            api_url = f"{debit_bank_link}do_transfer/"
            responses.append(make_transfer(api_url, data, headers))
        else:
            with transaction.atomic():
                for bank in bank_array:
                    api_url = f"{bank['link']}do_transfer/"
                    headers = {'API-Token': bank['token']}
                    responses.append(make_transfer(api_url, data, headers))
        return HttpResponse(responses)
    else: 
        return HttpResponseBadRequest(response)

def make_transfer(api_url, data, headers):
    response = requests.post(api_url, data=data, headers=headers)
    response_data = response.text
    if response.status_code == 200:
        return response_data
    else:
        return response_data

def check_account(account_id, api_url, headers):
    response = requests.get(api_url, params={"account_id": account_id}, headers=headers)
    if response.status_code == 200:
        return HttpResponse(response)
    else:
        return HttpResponseBadRequest(response)





