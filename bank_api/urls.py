from django.urls import path
from . import views


urlpatterns = [
    path('before_transfer/', views.before_transfer, name='before_transfer'),
]
