""" from django.db import models, transaction
from django.contrib.auth.models import User
from django.core.validators import MinLengthValidator
#from .errors import InsufficientFunds
from django.db.models.query import QuerySet
from decimal import Decimal
from django.conf import settings

class Bank(models.Model):
    name = models.CharField(max_length=100) """

""" class Customer(models.Model):
    bank = models.ForeignKey(Bank, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    account_balance = models.DecimalField(max_digits=10, decimal_places=2) """

""" class Rank(models.TextChoices):
    basic = "basic",
    silver = "silver",
    gold = "gold"

class Customer(models.Model):
    bank = models.ForeignKey(Bank, on_delete=models.CASCADE)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone_number = models.CharField(max_length=8, unique=True, validators=[MinLengthValidator(8, 'Phone number must contain 8 characters')])
    rank = models.CharField(max_length=256, choices=Rank.choices, default=2)

    def full_name(self) -> str:
        return f'{self.user.first_name} {self.user.last_name}'

    def username(self): return self.user.username

    def email(self): return self.user.email

    def __str__(self):
        return f'{self.user.pk} :: {self.user} :: {self.rank}' """

"""     @property
    def accounts(self) -> QuerySet:
        return Account.objects.filter(user=self.user)

    @property
    def can_make_loan(self) -> bool:
        return self.rank != "basic" # need to repair this!!!!!!

    @property
    def default_account(self) -> Account:
        return Account.objects.filter(user=self.user).first()

    def make_loan(self, amount):
        assert self.can_make_loan, 'User rank does not allow for making loans.'
        assert amount >= 0, 'Negative amount not allowed for loan.'
        loan = Account.objects.create(user=self.user, account_type=AccountTypes.loan_account)
        Transaction.transfer(
            amount,
            loan,
            f'Loan paid out to account {self.default_account}',
            self.default_account,
            f'Credit from loan {loan.pk}: {loan.account_type}',
            is_loan=True
        ) """

    
