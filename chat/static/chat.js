// Set focus on the room input field
document.querySelector("#roomInput").focus();

// When the user types in the room input field and hits enter,
// simulate a click on the room connect button
document.querySelector("#roomInput").onkeyup = function (event) {
  if (event.keyCode === 13) {
    document.querySelector("#roomConnect").click();
  }
};

// When the user clicks the room connect button,
// navigate to the chat room with the name specified in the input field
document.querySelector("#roomConnect").onclick = function () {
  // Get the room name and replace any spaces with underscores
  let roomName = document.querySelector("#roomInput").value.replace(/ /g, "_");
  window.location.pathname = `chat/${roomName}/`;
};

// When the user selects a room from the dropdown menu,
// navigate to the chat room with the selected name
document.querySelector("#roomSelect").onchange = function () {
  // Get the room name by splitting the value at the space and taking the first part
  let roomName = document.querySelector("#roomSelect").value.split(" (")[0];
  window.location.pathname = `chat/${roomName}/`;
};

// When the user clicks on a predefined room name,
// navigate to the chat room with the selected name
const predefinedRooms = document.querySelectorAll(".room-name-predefined");
predefinedRooms.forEach((room) => {
  room.addEventListener("click", function () {
    // Get the room name and replace any spaces with underscores
    let roomName = this.value.replace(/ /g, "_");
    window.location.assign(`/chat/${roomName}/`);
  });
});
