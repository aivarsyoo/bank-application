from django.shortcuts import render
from chat.models import Room
from django.contrib.auth.decorators import login_required


@login_required
def chat_view(request):
    return render(request, 'chat.html', {
        'rooms': Room.objects.all(),
    })


@login_required
def room_view(request, room_name):
    chat_room, created = Room.objects.get_or_create(name=room_name)
    return render(request, 'chat_room.html', {
        'room': chat_room,
    })
