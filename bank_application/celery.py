import os
from celery import Celery

app = Celery("bank_application")
app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks()