from .base import *

SESSION_ENGINE = 'django.contrib.sessions.backends.cache'

CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': 'redis://redis:6379/0',
        #'LOCATION': 'redis://localhost:6379/0',
        'OPTIONS': {
            'CLIENT_CLASS': 'django_redis.client.DefaultClient',
            'SESSION_CACHE_ALIAS': 'session',
            'SESSION_CACHE_PREFIX': 'bank2:session:',
        }
    }
}

SESSION_COOKIE_NAME = 'sessionid_bank2'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'bank2',
        'USER': 'user',
        'PASSWORD': 'password',
        'HOST': 'db2',
        'PORT': '5432',
    }
}