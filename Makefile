# DOCKER COMMANDS

up:
	docker-compose up

core-up:
	docker-compose up -d

build:
	docker-compose build

pull:
	docker-compose pull

down:
	docker-compose down --remove-orphans

down-rm-volumes:
	docker-compose down -v

logs:
	docker-compose logs -f

core-logs:
	docker-compose -f logs -f --tail 100

full-rebuild:
	docker-compose build --pull --no-cache

# DJANGO COMMANDS

bank1-makemigrations:
	DJANGO_SETTINGS_MODULE=bank_application.settings.bank1 python manage.py makemigrations

bank2-makemigrations:
	DJANGO_SETTINGS_MODULE=bank_application.settings.bank2 python manage.py makemigrations

bank1-migrate:
	DJANGO_SETTINGS_MODULE=bank_application.settings.bank1 python manage.py migrate

bank2-migrate:
	DJANGO_SETTINGS_MODULE=bank_application.settings.bank2 python manage.py migrate

bank1-runserver:
	DJANGO_SETTINGS_MODULE=bank_application.settings.bank1 python manage.py runserver 8000

bank2-runserver:
	DJANGO_SETTINGS_MODULE=bank_application.settings.bank2 python manage.py runserver 8001

bank1-createsuperuser:
	DJANGO_SETTINGS_MODULE=bank_application.settings.bank1 python manage.py createsuperuser

bank2-createsuperuser:
	DJANGO_SETTINGS_MODULE=bank_application.settings.bank2 python manage.py createsuperuser

bank1-demodata:
	DJANGO_SETTINGS_MODULE=bank_application.settings.bank1 python manage.py demodata

bank2-demodata:
	DJANGO_SETTINGS_MODULE=bank_application.settings.bank2 python manage.py demodata

bank1-worker:
	DJANGO_SETTINGS_MODULE=bank_application.settings.bank1 celery -A bank_application worker --loglevel=info

bank2-worker:
	DJANGO_SETTINGS_MODULE=bank_application.settings.bank2 celery -A bank_application worker --loglevel=info

bank1-beat:
	DJANGO_SETTINGS_MODULE=bank_application.settings.bank1 celery -A bank_application beat --loglevel=info

bank2-beat:
	DJANGO_SETTINGS_MODULE=bank_application.settings.bank2 celery -A bank_application beat --loglevel=info
